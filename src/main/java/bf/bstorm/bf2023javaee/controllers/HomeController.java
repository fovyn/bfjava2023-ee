package bf.bstorm.bf2023javaee.controllers;

import bf.bstorm.bf2023javaee.lib.di.Controller;
import bf.bstorm.bf2023javaee.lib.di.PathVariable;
import bf.bstorm.bf2023javaee.lib.di.RequestMapping;
import bf.bstorm.bf2023javaee.lib.http.HttpMethods;

import java.util.Arrays;
import java.util.Map;

@Controller
@RequestMapping(path = "/home")
public class HomeController {

    @RequestMapping(method = HttpMethods.GET, path = "")
    public String indexAction(Map<String, Object> model) {
        model.put("user", Arrays.asList("Flavian", "Gregory", "Le Commercial"));

        return "home/index";
    }

    @RequestMapping(method = HttpMethods.GET, path = "/(?<id>\\d+)")
    public String detailAction(Map<String, Object> model, @PathVariable(name = "id") Integer id) {
        model.put("id", id);

        return "home/detail";
    }

    @RequestMapping(method = HttpMethods.POST, path = "")
    public String createAction(Map<String, Object> model) {

        return "redirect:/home";
    }
}
