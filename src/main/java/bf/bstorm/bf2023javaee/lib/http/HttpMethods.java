package bf.bstorm.bf2023javaee.lib.http;

public enum HttpMethods {
    GET("GET"),
    POST("POST");

    String method;

    HttpMethods(String method) {
        this.method = method;
    }
}
