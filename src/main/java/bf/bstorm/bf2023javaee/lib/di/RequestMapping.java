package bf.bstorm.bf2023javaee.lib.di;

import bf.bstorm.bf2023javaee.lib.http.HttpMethods;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestMapping {
    HttpMethods[] method() default {};
    String[] path();
}
