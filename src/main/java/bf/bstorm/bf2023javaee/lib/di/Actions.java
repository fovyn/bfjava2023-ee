package bf.bstorm.bf2023javaee.lib.di;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Actions {
    private final Class<?> ctrl;
    private final String action;
    private final Pattern pattern;

    public Actions(Class<?> ctrl, String action, String uri) {
        this.ctrl = ctrl;
        this.action = action;
        this.pattern = Pattern.compile(uri);
    }

    public Class<?> getCtrl() {
        return ctrl;
    }

    public String getAction() {
        return action;
    }

    public boolean isRequestUri(String uri) {
        return pattern.matcher(uri).matches();
    }

    public Matcher getMatcher(String uri) {
        return pattern.matcher(uri);
    }
}
