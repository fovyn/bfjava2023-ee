package bf.bstorm.bf2023javaee.lib.di;

import bf.bstorm.bf2023javaee.lib.http.HttpMethods;
import jakarta.ejb.Singleton;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

@Singleton
public class DiContainer {
    private Map<HttpMethods, List<Actions>> actions = new HashMap<>();
    private Map<Class<?>, Object> controllers = new HashMap<>();
    private Map<Class<?>, Object> services = new HashMap<>();

    public DiContainer() {
        Set<Class<?>> services = new Reflections("bf.bstorm.bf2023javaee").getTypesAnnotatedWith(Service.class);
        Set<Class<?>> controllers = new Reflections("bf.bstorm.bf2023javaee").getTypesAnnotatedWith(Controller.class);

        for(Class<?> sClass: services) {
            this.addService(sClass);
        }

        for(Class<?> cClass: controllers) {
            this.addController(cClass);
            this.addRoutes(cClass);
        }
    }

    private void addRoutes(Class<?> cClass) {
        String prefix = "";
        if (cClass.isAnnotationPresent(RequestMapping.class)) {
            RequestMapping requestMapping = cClass.getAnnotation(RequestMapping.class);
            prefix = requestMapping.path()[0];
        }

        Method[] methods = cClass.getDeclaredMethods();
        for(Method method: methods) {
            if (method.isAnnotationPresent(RequestMapping.class)) {
                RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
                String path = requestMapping.path()[0];
                Actions action = new Actions(cClass, method.getName(), prefix+ path);

                if (!this.actions.containsKey(requestMapping.method()[0])) {
                    this.actions.put(requestMapping.method()[0], new ArrayList<>());
                }
                this.actions.get(requestMapping.method()[0]).add(action);
            }
        }
    }

    private <T> T addController(Class<T> cClass)  {
        Constructor<?> constructor = cClass.getConstructors()[0];
        List<Object> pValues = new ArrayList<>();

        Parameter[] parameters = constructor.getParameters();
        for(Parameter parameter: parameters) {
            Class<?> type = parameter.getType();
            Object pValue = this.getService(type);
            pValues.add(pValue);
        }

        try {
            T instance = (T) constructor.newInstance(pValues.toArray(Object[]::new));
            this.controllers.put(cClass, instance);
            return instance;
        } catch (Exception e) {
            throw new RuntimeException();
        }

    }
    public <T> T addService(Class<T> tClass) {
        Constructor<?> constructor = tClass.getConstructors()[0];
        try {
            List<Object> pValues = new ArrayList<>();
            Parameter[] parameters = constructor.getParameters();
            for(Parameter parameter: parameters) {
                Class<?> type = parameter.getType();
                Object pValue = this.getService(type);
                pValues.add(pValue);
            }
            T instance = (T) constructor.newInstance(pValues.toArray(Object[]::new));
            this.services.put(tClass, instance);
            return instance;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T getService(Class<T> tClass) {
        if (!this.services.containsKey(tClass)) {
            return this.addService(tClass);
        }
        return (T) this.services.get(tClass);
    }
    public Object getController(Class<?> cClass) {
        return this.controllers.get(cClass);
    }

    public Optional<Actions> getAction(HttpMethods methods, String uri) {
        return this.actions.get(methods).stream()
                .filter(it -> it.isRequestUri(uri))
                .findFirst();
    }

}
