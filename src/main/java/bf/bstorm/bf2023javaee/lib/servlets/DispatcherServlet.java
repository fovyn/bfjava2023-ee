package bf.bstorm.bf2023javaee.lib.servlets;

import bf.bstorm.bf2023javaee.lib.di.Actions;
import bf.bstorm.bf2023javaee.lib.di.DiContainer;
import bf.bstorm.bf2023javaee.lib.di.PathVariable;
import bf.bstorm.bf2023javaee.lib.freemarker.ModelAndView;
import bf.bstorm.bf2023javaee.lib.http.HttpMethods;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

@WebServlet(name = "dispatcher", urlPatterns = {"/*"})
public class DispatcherServlet extends HttpServlet {
    private static final String BASE_PATH = "/bf2023-javaee-1.0-SNAPSHOT";
    @EJB
    private DiContainer diContainer;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI().replace(BASE_PATH, "");

        Actions action = diContainer.getAction(HttpMethods.GET, uri).orElseThrow(RuntimeException::new);

        executeAction(uri, action, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI().replace(BASE_PATH, "");

        Actions action = diContainer.getAction(HttpMethods.POST, uri).orElseThrow(RuntimeException::new);

        executeAction(uri, action, resp);
    }

    private void executeAction(String uri, Actions action, HttpServletResponse resp) {
        Object controller = diContainer.getController(action.getCtrl());

        try {
            Map<String, Object> model = new HashMap<>();

            List<Object> parameters = new ArrayList<>();
            parameters.add(model);

            Matcher matcher = action.getMatcher(uri);
            System.out.println(matcher.matches());
            System.out.println(matcher.group("id"));
            Method method = Arrays.stream(action.getCtrl().getDeclaredMethods())
                    .filter(it -> it.getName().equals(action.getAction()))
                    .findFirst()
                    .orElseThrow(RuntimeException::new);
            List<PathVariable> pathParameters = Arrays.stream(method.getParameters()).filter(it -> it.isAnnotationPresent(PathVariable.class)).map(it -> it.getAnnotation(PathVariable.class)).collect(Collectors.toList());
            for(PathVariable parameter : pathParameters) {
                Object value = matcher.group(parameter.name());
                parameters.add(value);
            }

            String view = (String) method.invoke(controller, parameters.toArray(Object[]::new));
            if (view.startsWith("redirect:")) {
                resp.sendRedirect(BASE_PATH+ view.replace("redirect:", ""));
                return;
            }
            ModelAndView mv = new ModelAndView(view, model);

            mv.display(resp.getOutputStream());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
