package bf.bstorm.bf2023javaee.services;

import bf.bstorm.bf2023javaee.lib.di.Service;
import bf.bstorm.bf2023javaee.repositories.PersonRepository;

@Service
public class PersonService {

    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public void sayHello() {
        System.out.println("Hello there!");
    }
}
